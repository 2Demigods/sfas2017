﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Transform player;
    private Gun gun;
    private float time;

    public GameObject bullet;

    public float reloadTime;

    public void Start()
    {
        time = 0f;
        player = FindObjectOfType<Player>().transform;
        gun = new Gun(this.transform, player.GetComponent<Player>());
    }

    public void Update()
    {
        if (GameManager.state == GameManager.State.Playing)
        {
            transform.LookAt(player, Vector3.up);
            if (time >= reloadTime)
            {
                gun.Shoot(0f, 1, 100f, bullet);
                time -= reloadTime;
            }
            time += Time.deltaTime;
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Bullet")
        {
            Destroy(col.gameObject);
            Destroy(this.gameObject);
        }
        //Debug.Log(col.tag);
    }

}

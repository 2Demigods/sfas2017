﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bullets : MonoBehaviour
{
	
    private int myBullets = 0;
    private int myAmmo = 0;
    private string myWeapon = "";
    [SerializeField]
    private Text text;

    // Update is called once per frame
    void Update()
    {
        if (Player.ammo != myAmmo)
        {
            myAmmo = Player.ammo;
            text.text = "Bullets/Shot: " + myBullets + "\n Ammo: " + myAmmo + "\n Weapon: " + myWeapon;
        }
        if (Player.numberOfBullets != myBullets)
        {
            myBullets = Player.numberOfBullets;
            text.text = "Bullets/Shot: " + myBullets + "\n Ammo: " + myAmmo + "\n Weapon: " + myWeapon;
        }
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillOrbOnTouch : MonoBehaviour
{

    private void OnCollisionEnter(Collision col)
    {
        if (col.collider.tag == "Bullet" || col.collider.tag == "Orb")
        {
            Destroy(col.gameObject);
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Bullet" || col.tag == "Orb")
        {
            Destroy(col.gameObject);
        }

        //Debug.Log(col.tag);
    }
}

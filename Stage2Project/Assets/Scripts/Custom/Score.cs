﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{

    public Text txt;

    // Update is called once per frame
    void Update()
    {
        txt.text = "Your Score is:\n" + (long)Mathf.Floor(Player.Score);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    // Use this for initialization

    public class SpawnObject
    {
        public GameObject placeHolder;
        public GameObject instance;
        public float time;
    }

    public float spawnTimer = 2f;
    public float timeToSpawn = 1f;

    public int numberOfSpawnsPerWave = 3;
    [SerializeField]
    private int waveDuration = 30;
    [SerializeField]
    private GameObject[] SpawnPrefabs;
    [SerializeField]
    private GameObject placeHolder;
    private static List<SpawnObject> objs;

    void Start()
    {
        objs = new List<SpawnObject>();
    }

    private float nextSpawn = 0f;

    public static void newGame()
    {
        objs.Clear();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.state == GameManager.State.Playing)
        {
            
            foreach (SpawnObject item in objs)
            {
                if (item != null && item.time <= 0f)
                {
                    GameObject go = Instantiate(item.instance);
                    go.transform.position = item.placeHolder.transform.position;
                    go.transform.SetParent(this.transform);
                    Destroy(item.placeHolder);
                }
            }
            objs.RemoveAll(x => x.time <= 0f);
            objs.ForEach(x => x.time -= Time.deltaTime);
            
            if (nextSpawn >= spawnTimer)
            {
                nextSpawn -= spawnTimer;
                Debug.Log((((long)Mathf.Floor(Player.Score)) / 30));
                for (int i = 0; i < (numberOfSpawnsPerWave * (1 + (((long)Mathf.Floor(Player.Score)) / waveDuration))); i++)
                {
                    SpawnObject s = new SpawnObject();
                    s.time = timeToSpawn;
                    int indexToSpawn = Random.Range(0, SpawnPrefabs.Length);
                    s.instance = SpawnPrefabs[indexToSpawn];
                    objs.Add(s);
                    GameObject placeInst = Instantiate(placeHolder);
                    placeInst.transform.SetParent(this.transform);
                    s.placeHolder = placeInst;
                }
                waveDuration++;
            }
            nextSpawn += Time.deltaTime;
        }
    }
}

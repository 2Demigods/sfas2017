﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitController : MonoBehaviour
{

    public Transform target;
    public Transform[] orbiters;
    public Transform parentOfOrb;
    public Vector3[] relPos;

    public Transform[] oldOrb;

    public float orbitSpeed = 10f;
    public float orbitDistance = 3f;
    public float attactionForce = 1f;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(orbitWithSpeed());
        List<Transform> a = new List<Transform>();
        for (int i = 0; i < parentOfOrb.childCount; i++)
        {
            a.Add(parentOfOrb.GetChild(i));
        }
        orbiters = a.ToArray();
        oldOrb = new Transform[0];
        relPos = new Vector3[orbiters.Length];
        for (int i = 0; i < orbiters.Length; i++)
        {
            relPos[i] = orbiters[i].position - target.position;
        }
    }
	
    // Update is called once per frame
    /*void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Transform[] aux = orbiters;
            List<Transform> ts = new List<Transform>(aux);
            ts.RemoveAll(x => x == null);
            aux = ts.ToArray();
            ts = new List<Transform>(oldOrb);
            ts.RemoveAll(x => x == null);
            oldOrb = ts.ToArray();
            orbiters = oldOrb;
            oldOrb = aux;
            for (int i = 0; i < oldOrb.Length; i++)
            {
                if (oldOrb[i] != null)
                    oldOrb[i].GetComponent<Rigidbody>().velocity = (oldOrb[i].position - target.position).normalized * 20f; 
            }
            for (int i = 0; i < orbiters.Length; i++)
            {
                if (orbiters[i] != null)
                    relPos[i] = orbiters[i].position - target.position;
            }
        }

    }*/

    private IEnumerator orbitWithSpeed()
    {
        while (true)
        {
            /*
            foreach (Transform orbiter in orbiters)
            {
                Vector3 distance = target.position - orbiter.position;
                Vector3 speedDir = Vector3.Cross(distance.normalized, Vector3.up).normalized;
                //orbiter.GetComponent<Rigidbody>().velocity = orbitSpeed * speedDir + (distance.normalized * ((distance.magnitude - orbitDistance)));
                /*
                if (((distance.magnitude - orbitDistance)) >= 1)
                    orbiter.GetComponent<Rigidbody>().velocity = orbitSpeed * (speedDir + distance.normalized * attactionForce * (1f / (distance.magnitude - orbitDistance)));
                else if (((distance.magnitude - orbitDistance)) >= 0)
                    orbiter.GetComponent<Rigidbody>().velocity = orbitSpeed * (speedDir + distance.normalized * attactionForce * ((distance.magnitude - orbitDistance)));
                else
                    orbiter.GetComponent<Rigidbody>().velocity = orbitSpeed * (speedDir + distance.normalized * attactionForce * ((distance.magnitude - orbitDistance)));
                
                orbiter.position = orbiter.position + distance.normalized * (distance.magnitude - orbitDistance);
                orbiter.RotateAround(target.position, Vector3.up, orbitSpeed * Time.deltaTime);
                orbiter.position = new Vector3(orbiter.position.x, 0f, orbiter.position.z);
            }
            */
            for (int i = 0; i < orbiters.Length; i++)
            {
                if (orbiters[i] != null)
                {
                    if (orbitDistance != 0f)
                        orbiters[i].position = target.position + relPos[i].normalized * orbitDistance;
                    orbiters[i].position = new Vector3(orbiters[i].position.x, 0f, orbiters[i].position.z);
                    orbiters[i].RotateAround(target.position, Vector3.up, orbitSpeed * Time.deltaTime);
                    relPos[i] = orbiters[i].position - target.position;
                }
            }

            yield return null;
        }
    }
}

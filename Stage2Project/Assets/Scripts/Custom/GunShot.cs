﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunShot : MonoBehaviour
{


    public GameObject bulletPrefab;
    public Camera c;
    public Transform target;
    public LineRenderer linerend;

    void Start()
    {
        target = Instantiate(bulletPrefab).transform;
        target.gameObject.layer = 0;
        target.gameObject.tag = "Untagged";
        target.name = "crosshairs";
        linerend = GetComponent<LineRenderer>();
        //target.GetComponent<Collider>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        Ray ray = c.ScreenPointToRay(Input.mousePosition);
        Vector3 v3 = ray.direction.normalized;
        RaycastHit rayhit;
        Physics.Raycast(ray, out rayhit);
        float distace = rayhit.distance;
        //v3.y = transform.position.y;
        v3 = ray.GetPoint(distace);
        v3.y = transform.position.y;
        target.position = v3;
        transform.LookAt(v3);
        linerend.numPositions = 2;
        linerend.SetPosition(0, this.transform.position);
        linerend.SetPosition(1, this.transform.forward * 5 + this.transform.position);

    }
    /* Vector3 direction = Vector3.zero;
        mBody.velocity = Vector3.zero;

        if (Input.GetKey(KeyCode.A) && !Physics.Raycast(transform.position, Vector3.left, 1.05f))
        {
            direction = -Vector3.right;
        } else if (Input.GetKey(KeyCode.D) && !Physics.Raycast(transform.position, Vector3.right, 1.05f))
        {
            direction = Vector3.right;
        }
        if (Input.GetKey(KeyCode.W) && !Physics.Raycast(transform.position, Vector3.forward, 1.05f))
        {
            direction += Vector3.forward;
        } else if (Input.GetKey(KeyCode.S) && !Physics.Raycast(transform.position, Vector3.back, 1.05f))
        {
            direction += -Vector3.forward;
        }
            
        if (Input.GetKeyDown(KeyCode.Space))
        {
            float anglePerBullet;
            if (angleOfSpread == 360f)
            {
                anglePerBullet = angleOfSpread / numberOfBullets;
                shootBullets(anglePerBullet, true);
            } else
            {
                anglePerBullet = angleOfSpread / (numberOfBullets - 1);
                shootBullets(anglePerBullet, false);
            }
        }

        mBody.velocity = (direction.normalized * Speed);
        *
        //Debug.Log(Input.mousePosition);
    }

    private void shootBullets(float anglePerBullet, bool is360)
    {
        if (is360)
        {
            for (int i = 0; i < numberOfBullets; i++)
            {
                GameObject bullet = (GameObject)Instantiate(bulletPrefab);
                bullet.transform.position = transform.position;
                float angle = (((i + 1) / 2) * anglePerBullet - transform.rotation.eulerAngles.y + 90f) * Mathf.Deg2Rad;
                anglePerBullet *= -1;
                Vector3 direction = new Vector3(Mathf.Cos(angle), 0f, Mathf.Sin(angle));
                bullet.transform.LookAt(transform.position + 10f * direction);
                bullets.Add(bullet);
            }
        } else
        {
            for (int i = 0; i < numberOfBullets; i++)
            {
                GameObject bullet = (GameObject)Instantiate(bulletPrefab);
                bullet.transform.position = transform.position;
                float angle = (i * anglePerBullet - (angleOfSpread / 2f) - transform.rotation.eulerAngles.y + 90f) * Mathf.Deg2Rad;
                Vector3 direction = new Vector3(Mathf.Cos(angle), 0f, Mathf.Sin(angle));
                bullet.transform.LookAt(transform.position + 10f * direction);
                bullets.Add(bullet);
                //bullet.GetComponent<Rigidbody>().velocity = direction * bulletVelocity;
            }
        }
    }

    private IEnumerator moveBullets()
    {
        while (true)
        {
            
            foreach (GameObject bullet in bullets)
            {
                if (Vector3.Distance(bullet.transform.position, transform.position) > 250f)
                {
                    Destroy(bullet);
                }
                if (bullet != null)
                {
                    bullet.transform.position += bullet.transform.forward.normalized * bulletVelocity * Time.deltaTime;
                }
            }
            //yield return null;
            yield return new WaitForFixedUpdate();
            bullets.RemoveAll(x => x == null);
        }
    }
    */
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldOrbit : MonoBehaviour
{

    public Transform target;
    public Transform[] orbiters;
    public Transform parentOfOrb;

    public float orbitSpeed = 10f;
    public float orbitDistance = 3f;
    public float attactionForce = 1f;

    private float degSep;

    // Use this for initialization
    void Start()
    {
       
        List<Transform> a = new List<Transform>();
        for (int i = 0; i < parentOfOrb.childCount; i++)
        {
            Transform aux = parentOfOrb.GetChild(i);
            aux.tag = "Untagged";
            aux.gameObject.layer = LayerMask.NameToLayer("Default");
            a.Add(aux);
        }
        orbiters = a.ToArray();
        degSep = 360f / orbiters.Length;
        StartCoroutine(orbitWithSpeed());
    }

    private IEnumerator orbitWithSpeed()
    {
        float time = 0f;
        while (true)
        {
            for (int i = 0; i < orbiters.Length; i++)
            {
                if (orbiters[i] != null)
                {
                    orbiters[i].position = new Vector3(Mathf.Cos((time + i * degSep) * Mathf.Deg2Rad) * orbitDistance + target.position.x, 0f, Mathf.Sin((time + i * degSep) * Mathf.Deg2Rad) * orbitDistance + target.position.z);
                    orbiters[i].LookAt(target);
                }
            }
            time += Time.deltaTime * 360f * orbitSpeed;
            if (time >= 360)
            {
                time -= 360;
            }
            yield return null;
        }
    }
}

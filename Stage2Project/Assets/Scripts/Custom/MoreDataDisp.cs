﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoreDataDisp : MonoBehaviour
{

    private float hpR = 999999;
    private float BulletR = 99999;
    private float angleOS = 999999;

    [SerializeField]
    private Text moreTxt;

    // Update is called once per frame
    void Update()
    {
        if (angleOS != Player.angleOfSpread)
        {
            angleOS = Player.angleOfSpread;
            moreTxt.text = "Angle Of Spread: " + angleOS + "\nHP regen: " + hpR + "\nBullet regen: " + BulletR;
        }
        if (BulletR != Player.bulletsPerSecond)
        {
            BulletR = Player.bulletsPerSecond;
            moreTxt.text = "Angle Of Spread: " + angleOS + "\nHP regen: " + hpR + "\nBullet regen: " + BulletR;
        }
        if (hpR != Player.HpPerSecond)
        {
            hpR = Player.HpPerSecond;
            moreTxt.text = "Angle Of Spread: " + angleOS + "\nHP regen: " + hpR + "\nBullet regen: " + BulletR;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun
{
    private Transform me;
    private MonoBehaviour meMono;
    private float maxDist;


    public Gun(Transform _me, MonoBehaviour _meMono)
    {
        me = _me;
        meMono = _meMono;
        maxDist = Mathf.Sqrt(Screen.width * Screen.width + Screen.height * Screen.height) + 20f;
    }

    public void Shoot(float angleOfSpread, int numberOfBullets, float bulletVelocity, GameObject bulletPrefab)
    {
        float anglePerBullet;
        List<Transform> bullets = new List<Transform>();
        if (angleOfSpread > 350f || numberOfBullets == 1)
        {
            anglePerBullet = angleOfSpread / numberOfBullets;
            for (int i = 0; i < numberOfBullets; i++)
            {
                GameObject bullet = GameObject.Instantiate(bulletPrefab);
                bullet.transform.position = me.position;
                float angle = (((i + 1) / 2) * anglePerBullet - me.rotation.eulerAngles.y + 90f) * Mathf.Deg2Rad;
                anglePerBullet *= -1;
                Vector3 direction = new Vector3(Mathf.Cos(angle), 0f, Mathf.Sin(angle));
                bullet.transform.LookAt(me.position + 10f * direction);
                bullets.Add(bullet.transform);
            }
        } else
        {
            anglePerBullet = angleOfSpread / (numberOfBullets - 1);
            for (int i = 0; i < numberOfBullets; i++)
            {
                GameObject bullet = GameObject.Instantiate(bulletPrefab);
                bullet.transform.position = me.position;
                float angle = (i * anglePerBullet - (angleOfSpread / 2f) - me.rotation.eulerAngles.y + 90f) * Mathf.Deg2Rad;
                Vector3 direction = new Vector3(Mathf.Cos(angle), 0f, Mathf.Sin(angle));
                bullet.transform.LookAt(me.position + 10f * direction);
                bullets.Add(bullet.transform);
            }
        }
        meMono.StartCoroutine(moveBullets(bulletVelocity, bullets));
    }

    private IEnumerator moveBullets(float bulletVelocity, List<Transform> bullets)
    {
        Vector3 lastme = me.position;
        while (bullets.Capacity > 0)
        {
            if (GameManager.state == GameManager.State.Playing)
            {
                foreach (Transform bullet in bullets)
                {
                    
                    if (Vector3.Distance(bullet.position, lastme) > maxDist)
                    {
                        GameObject.Destroy(bullet.gameObject);
                    }
                    if (bullet != null)
                    {
                        float moveDist = bulletVelocity * Time.fixedDeltaTime;
                        bullet.position += bullet.forward.normalized * moveDist;
                    }
                }
            }
            //yield return null;
            yield return new WaitForFixedUpdate();
            bullets.RemoveAll(x => x == null);
        }
    }



}

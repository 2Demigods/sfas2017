﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HpDisplay : MonoBehaviour
{

    private long maxHP;
    private long curHP;

    [SerializeField]
    private Text hpTxt;
    [SerializeField]
    private Image hpImage;
    [SerializeField]
    private RectTransform hpbar;
	
    // Update is called once per frame
    void Update()
    {
        if (maxHP != Player.maxHealth)
        {
            maxHP = Player.maxHealth;
            hpTxt.text = curHP + " / " + maxHP;
            float t = (float)curHP / (float)maxHP; 
            hpImage.rectTransform.localScale = new Vector3(t, hpbar.localScale.y, hpbar.localScale.z);
        }
        if (curHP != Player.currentHealth)
        {
            curHP = Player.currentHealth;
            hpTxt.text = curHP + " / " + maxHP;
            //float t = ((float)curHP) * ((float)((float)-hpbar.rect.width) / ((float)maxHP)) + ((float)hpbar.rect.width);
            float t = (float)curHP / (float)maxHP; 
            hpImage.rectTransform.localScale = new Vector3(t, hpbar.localScale.y, hpbar.localScale.z);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MyPlayer : MonoBehaviour
{
    [SerializeField]
    private float mSpeed;

    private Rigidbody mBody;

    void Awake()
    {
        mBody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        Vector3 direction = Vector3.zero;

        if (Input.GetKey(KeyCode.A) && !Physics.Raycast(transform.position, Vector3.left, 1.05f))
        {
            direction = -Vector3.right;
        } else if (Input.GetKey(KeyCode.D) && !Physics.Raycast(transform.position, Vector3.right, 1.05f))
        {
            direction = Vector3.right;
        }
        if (Input.GetKey(KeyCode.W) && !Physics.Raycast(transform.position, Vector3.forward, 1.05f))
        {
            direction += Vector3.forward;
        } else if (Input.GetKey(KeyCode.S) && !Physics.Raycast(transform.position, Vector3.back, 1.05f))
        {
            direction += -Vector3.forward;
        }
        mBody.velocity = (direction.normalized * mSpeed);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public enum State
    {
        Paused,
        Playing,
        GameOver

    }

    [SerializeField]
    private GameObject[] SpawnPrefabs;

    [SerializeField]
    private Player PlayerPrefab;

    [SerializeField]
    private Arena Arena;

    [SerializeField]
    private float timeBetweenSpawns;

    public Camera c;

    private List<GameObject> mObjects;
    private Player mPlayer;

    public static State state;

    private float mNextSpawn;

    void Awake()
    {
        mPlayer = Instantiate(PlayerPrefab);
        mPlayer.transform.parent = transform;
        mPlayer.gameObject.GetComponent<GunShot>().c = c;

        ScreenManager.OnNewGame += ScreenManager_OnNewGame;
        ScreenManager.OnExitGame += ScreenManager_OnExitGame;
        ScreenManager.onRestartGame += ScreenManager_onRestartGame;
    }

    void ScreenManager_onRestartGame()
    {
        state = State.Paused;
        Player.ammo = 100;
        Player.Score = 0f;
        Player.angleOfSpread = 0;
        Player.bulletsPerSecond = 0.5f;
        Player.currentHealth = 100;
        Player.maxHealth = 100;
        Player.numberOfBullets = 1;
        Spawner.newGame();
        GameObject[] toDelete = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject go in toDelete)
        {
            Destroy(go);
        }
        toDelete = GameObject.FindGameObjectsWithTag("Bullet");
        foreach (GameObject go in toDelete)
        {
            Destroy(go);
        }
    }

    void Start()
    {
        Arena.Calculate();
        mPlayer.enabled = false;
        state = State.Paused;
    }

    void Update()
    {
        if (state == State.Playing)
        {
            /* mNextSpawn -= Time.deltaTime;
            if (mNextSpawn <= 0.0f)
            {
                if (mObjects == null)
                {
                    mObjects = new List<GameObject>();
                }

                int indexToSpawn = Random.Range(0, SpawnPrefabs.Length);
                GameObject spawnObject = SpawnPrefabs[indexToSpawn];
                GameObject spawnedInstance = Instantiate(spawnObject);
                spawnedInstance.transform.parent = transform;
                mObjects.Add(spawnedInstance);
                mNextSpawn = TimeBetweenSpawns;
            }*/
        }
        if (Player.currentHealth <= 0)
        {
            state = State.GameOver;
        }
    }

    private void BeginNewGame()
    {
        if (mObjects != null)
        {
            for (int count = 0; count < mObjects.Count; ++count)
            {
                Destroy(mObjects[count]);
            }
            mObjects.Clear();
        }

        mPlayer.transform.position = new Vector3(0.0f, 0.5f, 0.0f);
        mNextSpawn = timeBetweenSpawns;
        mPlayer.enabled = true;
        state = State.Playing;
    }

    private void EndGame()
    {
        mPlayer.enabled = false;
        state = State.Paused;
    }

    private void ScreenManager_OnNewGame()
    {
        BeginNewGame();
    }

    private void ScreenManager_OnExitGame()
    {
        EndGame();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenManager : MonoBehaviour
{
    public delegate void GameEvent();

    public static event GameEvent OnNewGame;
    public static event GameEvent OnExitGame;
    public static event GameEvent onRestartGame;

    public enum Screens
    {
        TitleScreen,
        GameScreen,
        ResultScreen,
        PausedScreen,
        NumScreens

    }

    private Canvas[] mScreens;
    private Screens mCurrentScreen;

    void Awake()
    {
        mScreens = new Canvas[(int)Screens.NumScreens];
        Canvas[] screens = GetComponentsInChildren<Canvas>();
        for (int count = 0; count < screens.Length; ++count)
        {
            for (int slot = 0; slot < mScreens.Length; ++slot)
            {
                if (mScreens[slot] == null && ((Screens)slot).ToString() == screens[count].name)
                {
                    mScreens[slot] = screens[count];
                    break;
                }
            }
        }

        for (int screen = 1; screen < mScreens.Length; ++screen)
        {
            mScreens[screen].enabled = false;
        }

        mCurrentScreen = Screens.TitleScreen;
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameManager.state == GameManager.State.Playing)
            {
                GameManager.state = GameManager.State.Paused;
                TransitionTo(Screens.PausedScreen);
            } else
            {
                GameManager.state = GameManager.State.Playing;
                TransitionTo(Screens.GameScreen);
            }
        }
        if (GameManager.state == GameManager.State.GameOver)
        {
            EndGame();
        }
    }

    public void restartGame()
    {
        if (onRestartGame != null)
        {
            onRestartGame();
        }
        TransitionTo(Screens.TitleScreen);
    }

    public void StartGame()
    {
        if (OnNewGame != null)
        {
            OnNewGame();
        }

        TransitionTo(Screens.GameScreen);
    }

    public void EndGame()
    {
        if (OnExitGame != null)
        {
            OnExitGame();
        }

        TransitionTo(Screens.ResultScreen);
    }

    private void TransitionTo(Screens screen)
    {
        mScreens[(int)mCurrentScreen].enabled = false;
        mScreens[(int)screen].enabled = true;
        mCurrentScreen = screen;
    }

    public void unPause()
    {
        GameManager.state = GameManager.State.Playing;
        TransitionTo(Screens.GameScreen);
    }

    public void AddHpMax()
    {
        if (Player.ammo >= 10)
        {
            Player.ammo -= 10;
            Player.maxHealth += 10;
        }
    }

    public void AddHpReg()
    {
        if (Player.ammo >= 10)
        {
            Player.ammo -= 10;
            Player.HpPerSecond += 0.05f;
        }
    }

    public void AddBulletRegen()
    {
        if (Player.ammo >= 10)
        {
            Player.ammo -= 10;
            Player.bulletsPerSecond += 0.5f;
        }
    }

    public void AddBulletSpread()
    {
        if (Player.ammo >= 10)
        {
            Player.ammo -= 10;
            Player.angleOfSpread += 5f;
        }
    }

    public void AddBulletToShot()
    {
        if (Player.ammo >= 10)
        {
            Player.ammo -= 10;
            Player.numberOfBullets += 1;
        }
    }

    public void AddAmmoInf()
    {
        Player.ammo += 10000;
    }
}

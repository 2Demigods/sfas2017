﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MagnetizedByPlayer : MonoBehaviour
{
    public enum Type
    {
        Attract,
        Repel,
        KeepDistance

    }

    [SerializeField]
    private float RepelForce = 1000.0f;

    [SerializeField]
    private float MinimumDistance = 1.0f;

    [SerializeField]
    private float distanceMax = 1.0f;

    [SerializeField]
    private float distanceMin = 1.0f;

    [SerializeField]
    private Type MagnetizeType = Type.Repel;

    private Player mPlayer;
    private Rigidbody mBody;

    void Awake()
    {
        mPlayer = FindObjectOfType<Player>();
        mBody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if (GameManager.state == GameManager.State.Playing)
        {
            if (mPlayer != null)
            {
                if (Type.KeepDistance != MagnetizeType)
                {
                    Vector3 difference = MagnetizeType == Type.Repel ? transform.position - mPlayer.transform.position : mPlayer.transform.position - transform.position;
                    if (difference.magnitude <= MinimumDistance)
                    {
                        mBody.AddForce(difference * RepelForce * Time.deltaTime);
                    }
                } else
                {
                    Vector3 diff = transform.position - mPlayer.transform.position;
                    if (diff.magnitude > distanceMax)
                    {
                        mBody.AddForce((-1) * diff * RepelForce * Time.deltaTime);
                    } else if (diff.magnitude < distanceMin)
                    {
                        if (diff.magnitude < 3f && mBody.velocity != Vector3.zero)
                            mBody.velocity = Vector3.zero;
                        mBody.AddForce(((distanceMin - diff.magnitude) / Mathf.Pow((diff.magnitude), 1f)) * diff.normalized * RepelForce * distanceMin * Time.deltaTime);
                    }
                }
            }
        } else
        {
            mBody.velocity = Vector3.zero;
        }
    }
}

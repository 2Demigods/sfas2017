﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Player : MonoBehaviour
{
    [SerializeField]
    private float Speed;

    public AudioClip[] audios;

    public GameObject bulletPrefab;
    public static float angleOfSpread = 0f;
    public static float bulletSpeed = 125f;
    public static int numberOfBullets = 1;
    public static int ammo = 100;
    public static long currentHealth = 100;
    public static long maxHealth = 100;

    public static float bulletsPerSecond = 0.5f;
    public static float HpPerSecond = 0.04f;

    public static float Score = 0;

    private float addedBullets = 0f;
    private float addedHp = 0f;


    private Rigidbody mBody;
    private Gun gun;

    void Awake()
    {
        mBody = GetComponent<Rigidbody>();
        gun = new Gun(this.transform, this);

    }

    public void Update()
    {
        Vector3 direction = Vector3.zero;
        mBody.velocity = Vector3.zero;
        if (GameManager.state == GameManager.State.Playing)
        {
            if (Input.GetKey(KeyCode.A) && !Physics.Raycast(transform.position, Vector3.left, 1.05f, LayerMask.GetMask("Wall")))
            {
                direction = -Vector3.right;
            } else if (Input.GetKey(KeyCode.D) && !Physics.Raycast(transform.position, Vector3.right, 1.05f, LayerMask.GetMask("Wall")))
            {
                direction = Vector3.right;
            }
            if (Input.GetKey(KeyCode.W) && !Physics.Raycast(transform.position, Vector3.forward, 1.05f, LayerMask.GetMask("Wall")))
            {
                direction += Vector3.forward;
            } else if (Input.GetKey(KeyCode.S) && !Physics.Raycast(transform.position, Vector3.back, 1.05f, LayerMask.GetMask("Wall")))
            {
                direction += -Vector3.forward;
            }
            mBody.velocity = (direction.normalized * Speed);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                int bulletsAfter = ammo - numberOfBullets;
                if (bulletsAfter >= 0)
                {
                    gun.Shoot(angleOfSpread, numberOfBullets, bulletSpeed, bulletPrefab);
                    ammo = bulletsAfter;
                    GetComponent<AudioSource>().PlayOneShot(audios[Random.Range(0, audios.Length)]);
                } else if (ammo > 0)
                {
                    gun.Shoot(angleOfSpread, ammo, bulletSpeed, bulletPrefab);
                    ammo = 0;
                    GetComponent<AudioSource>().PlayOneShot(audios[Random.Range(0, audios.Length)]);
                }
            }


            Score += Time.deltaTime;

            addedBullets += bulletsPerSecond * Time.deltaTime;
            if (currentHealth < maxHealth)
                addedHp += HpPerSecond * Time.deltaTime;
            else
                addedHp = 0f;
            if (addedBullets >= 1f)
            {
                ammo += (int)Mathf.Floor(addedBullets);
                addedBullets -= Mathf.Floor(addedBullets);
            }
            if (addedHp >= 1f)
            {
                currentHealth += (int)Mathf.Floor(addedHp);
                addedHp -= Mathf.Floor(addedHp);
            }

        }

    }


    public void OnCollisionEnter(Collision col)
    {
        if (col.collider.tag == "Bullet" || col.collider.tag == "Enemy")
        {
            currentHealth--;
        }
    }

    public void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Bullet" || col.tag == "Enemy")
        {
            currentHealth--;
        }
    }

}

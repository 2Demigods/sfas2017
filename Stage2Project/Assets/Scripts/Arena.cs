﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Arena : MonoBehaviour
{
    [SerializeField]
    private Camera Cam;
    [SerializeField]
    private GameObject Wall;
    [SerializeField]
    private Transform[] walls;

    public static float Width { get; private set; }

    public static float Height { get; private set; }

    void Update()
    {
#if UNITY_EDITOR 
        if (!Application.isPlaying)
        {
            Calculate();
        }
#endif
    }

    public void Calculate()
    {
        if (Cam != null)
        {
            Height = CameraUtils.FrustumHeightAtDistance(Cam.farClipPlane - 1.0f, Cam.fieldOfView);
            Width = Height * Cam.aspect;
            transform.localScale = new Vector3(Width * 0.1f, 1.0f, Height * 0.1f);
            transform.position = new Vector3(Cam.transform.position.x, Cam.transform.position.y + Cam.transform.forward.normalized.y * (Cam.farClipPlane - 1.0f), Cam.transform.position.z);

            walls[0].position = transform.position + new Vector3(0f, 0f, Height / 2f + 1f); //N
            walls[0].localScale = new Vector3(Width, 1f, 5f);

            walls[1].position = transform.position + new Vector3(0f, 0f, -Height / 2f - 1f); //S
            walls[1].localScale = new Vector3(Width, 1f, 5f);

            walls[2].position = transform.position + new Vector3(Width / 2f + 1f, 0f, 0f); //E
            walls[2].localScale = new Vector3(5f, 1f, Height);

            walls[3].position = transform.position + new Vector3(-Width / 2f - 1f, 0f, 0f); //W
            walls[3].localScale = new Vector3(5f, 1f, Height);
        }
    }
}
